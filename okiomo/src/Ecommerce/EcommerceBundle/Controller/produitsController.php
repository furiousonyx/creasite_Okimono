<?php

namespace Ecommerce\EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class produitsController extends Controller {

    public function produitsAction($name) {
        return $this->render('EcommerceEcommerceBundle:Default:produit/layout/produit.html.twig');
    }

    public function presentationAction() {
        return $this->render('EcommerceBundle:Default:produits/layout/presentation.html.twig');
    }

}
