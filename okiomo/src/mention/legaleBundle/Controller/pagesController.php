<?php

namespace mention\legaleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class pagesController extends Controller
{
    public function pageAction($id)
    {
        return $this->render('mentionlegaleBundle:Default:/pageslayout/pages.html.twig');
    }
}
