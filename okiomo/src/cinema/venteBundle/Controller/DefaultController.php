<?php

namespace cinema\venteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('cinemaventeBundle:Default:index.html.twig', array('name' => $name));
    }
}
