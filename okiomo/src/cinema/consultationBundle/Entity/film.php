<?php

namespace cinema\consultationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * film
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class film
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=30)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=4)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="duree", type="string", length=5)
     */
    private $duree;

    /**
     * @var string
     *
     * @ORM\Column(name="resume", type="text")
     */
    private $resume;

    /**
     * @var string
     *
     * @ORM\Column(name="recettes", type="string", length=8)
     */
    private $recettes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return film
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return film
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    
        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set duree
     *
     * @param string $duree
     * @return film
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;
    
        return $this;
    }

    /**
     * Get duree
     *
     * @return string 
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set resume
     *
     * @param string $resume
     * @return film
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    
        return $this;
    }

    /**
     * Get resume
     *
     * @return string 
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set recettes
     *
     * @param string $recettes
     * @return film
     */
    public function setRecettes($recettes)
    {
        $this->recettes = $recettes;
    
        return $this;
    }

    /**
     * Get recettes
     *
     * @return string 
     */
    public function getRecettes()
    {
        return $this->recettes;
    }
}
