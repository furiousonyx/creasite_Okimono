<?php

namespace cinema\consultationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use cinema\consultationBundle\Entity\film;
use cinema\consultationBundle\Entity\studio;

class DefaultController extends Controller {

    public function indexAction($name) {
        return $this->render('cinemaconsultationBundle:Default:index.html.twig', array('name' => $name));
    }

    public function listefilmAction() {
        return $this->render('cinemaconsultationBundle:Default:listefilm.html.twig', array());
    }

    public function affichefilmAction($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $film = new film();
        $film = $entityManager->find('cinemaconsultationBundle:film', $id);
        return $this->render('cinemaconsultationBundle:Default:unfilm.html.twig', array('unFilm' => $film));
    }

    public function affichefilm2Action($id) {

        $entityManager = $this->getDoctrine()->getManager();
        $film = $entityManager->find('cinemaconsultationBundle:film', $id);

        return $this->render('cinemaconsultationBundle:Default:index.html.twig', array('unFilm' => $film));
    }

    public function affichelesfilmsAction() {

        $entityManager = $this->getDoctrine()->getManager();
        $films = new film();

        $films = $entityManager->getRepository('cinemaconsultationBundle:film')->findAll();
        /* foreach ($films as $film) {
          echo 'Titre :' . $film->getTitle() . '<br>';
          echo 'Résume :' . $film->getResume() . '<br>';
          echo 'Duree :' . $film->getDuree() . ' minute <br>';
          echo 'Pays :' . $film->getPays() . '<br>';
          echo 'Recette :' . $film->getRecettes() . ' yen <br>';
          } */
        return $this->render('cinemaconsultationBundle:studio:affichefilmetstudio.html.twig', array('film' => $films));
    }

    public function affichelesfilms2Action() {
        $entityManager = $this->getDoctrine()->getManager();
        $films = new film();

        $films = $entityManager->getRepository('cinemaconsultationBundle:film')->findAll();

        return $this->render('cinemaconsultationBundle:Default:listefilm.html.twig', array('film' => $films));
    }

    public function ajoutAction() {
        $entityManager = $this->getDoctrine()->getManager();

        //insertion du 1er studio

        $studio1 = new studio();
        $studio1->setNom("Toei animation");
        $studio1->setPays("France");
        $studio1->setCa(2000000);
        $entityManager->persist($studio1);
        $entityManager->flush();

        //insertion du 2eme studio
        $studio2 = new studio();
        $studio2->setNom("funimation");
        $studio2->setPays("France");
        $studio2->setCa(2000000);
        $entityManager->persist($studio2);
        $entityManager->flush();


        $film = new film();
        $film->setTitre("bla");
        $film->setDuree(35);
        $film->setPays("FR");
        $film->setResume("un long resume");
        $film->setRecettes(30);
        $entityManager->persist($film);
        $entityManager->flush();

        $film1 = new film();
        $film1->setTitre("bla bla");
        $film1->setDuree(35);
        $film1->setPays("FR");
        $film1->setResume("un long resume");
        $film1->setRecettes(30);
        $entityManager->persist($film1);
        $entityManager->flush();
        return $this->render('cinemaconsultationBundle:studio:ajoutstudiofilm.html.twig', array());
    }

    public function accueilAction() {
        $entityManager = $this->getDoctrine()->getManager();
        $films = new film();

        $films = $entityManager->getRepository('cinemaconsultationBundle:film')->findAll();

        $studios = new studio();

        $studios = $entityManager->getRepository('cinemaconsultationBundle:studio')->findAll();
        return $this->render('cinemaconsultationBundle:Default:index1.html.twig', array('film' => $films, 'studio' => $studios));
    }

    public function affichestudioAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $studios = new studio();

        $studios = $entityManager->find('cinemaconsultationBundle:studio', $id);
        return $this->render('cinemaconsultationBundle:Default:unstudio.html.twig', array('unstudio' => $studios));
    }

    function ajouterAction() {
        // création de l'objet Film
        $leFilm = new film();

        $formBuilder = $this->createFormBuilder($leFilm)
                ->add('titre', 'text', array('label' => 'Titre'))
                ->add('duree', 'text', array('label' => 'Durée'))
                ->add('pays', 'text', array('label' => 'Pays'))
                ->add('resume', 'text', array('label' => 'Résumé'))
                ->add('recettes', 'text', array('label' => 'Recettes'));
        //->add('Enregistrer', 'submit');
// génération du formulaire
        $form = $formBuilder->getForm();
//récupération de la requète retournée par le submit du formulaire
        $request = $this->get('request');
// vérification : la requête est-elle de type POST
        if ($request->getMethod() == 'POST') {
            /* lien requête <-> formulaire -> les données du formulaire (le nouveau film saisi)
              sont désormais dans la variable $leFilm */
            $form->bind($request);
// vérification : si les valeurs récupérées sont correctement saisies
            if ($form->isValid()) {
// enregistrement du nouveau film dans la BDD
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($leFilm);
                $entityManager->flush();
// puis appel au template permettant d'éditer ce nouveau film
// définition d'un message flash
                $this->get('session')->getFlashBag()->add('info', 'Film enregistré');
// => on effectue ici une redirection vers une autre page (route)
                return $this->redirect($this->generateUrl('cinemaconsultation_affichefilm2', array('id' => $leFilm->getId())));
            }
        }
// appel à la vue avec en paramètre le formulaire créé
        return $this->render('cinemaconsultationBundle:Film:ajouter.html.twig', array('form' => $form->createView()));
    }

    function modifierfilmAction($id) {

        $entityManager = $this->getDoctrine()->getManager();
        $leFilm = $entityManager->find('cinemaconsultationBundle:film', $id);
// création du FormBuilder avec les champs que l'on souhaite (sauf l'id)
        $formBuilder = $this->createFormBuilder($leFilm)
                ->add('titre', 'text', array('label' => 'Titre'))
                ->add('duree', 'text', array('label' => 'Durée'))
                ->add('pays', 'text', array('label' => 'Pays'))
                ->add('resume', 'text', array('label' => 'Résumé'))
                ->add('recettes', 'text', array('label' => 'Recettes'));

// génération du formulaire
        $form = $formBuilder->getForm();
//récupération de la requète retournée par le submit du formulaire
        $request = $this->get('request');
// vérification : la requête est-elle de type POST
        if ($request->getMethod() == 'POST') {
            /* lien requête <-> formulaire -> les données du formulaire (le nouveau film saisi)
              sont désormais dans la variable $leFilm */
            $form->bind($request);
// vérification : si les valeurs récupérées sont correctement saisies
            if ($form->isValid()) {
// enregistrement du nouveau film dans la BDD
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($leFilm);
                $entityManager->flush();
// puis appel au template permettant d'éditer ce nouveau film
// définition d'un message flash
                $this->get('session')->getFlashBag()->add('info', 'Film modifié');
// => on effectue ici une redirection vers une autre page (route)
                return $this->redirect($this->generateUrl('cinemaconsultation_affichefilm2', array('id' => $leFilm->getId())));
            }
        }
        return $this->render('cinemaconsultationBundle:Film:modifier.html.twig', array('form' => $form->createView(), 'film' => $leFilm));
    }

    function supprimerfilmAction($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $leFilm = $entityManager->find('cinemaconsultationBundle:film', $id);
        $formBuilder = $this->createFormBuilder($leFilm);

        $form = $formBuilder->getForm();

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                $entityManager->remove($leFilm);
                $entityManager->flush();
                $this->get('session')->getFlashBag()->add('info', 'Film supprimé');
                return $this->redirect($this->generateUrl('cinemaconsultation_homepage', array('id' => $leFilm->getId())));
            }
        }
        return $this->render('cinemaconsultationBundle:Film:supprimer.html.twig', array('form' => $form->createView(), 'unFilm' => $leFilm));
    }
    function affichelesfilmsfrancaisAction() {
        $entityManager = $this->getDoctrine()->getManager();
        $films = new film();

        $films = $entityManager->getRepository('cinemaconsultationBundle:film')->findBy(array('pays' => 'FR'));

        return $this->render('cinemaconsultationBundle:Default:listefilm.html.twig', array('film' => $films));
    }

}

